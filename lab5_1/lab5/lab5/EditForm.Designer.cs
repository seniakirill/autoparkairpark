﻿namespace lab5
{
    partial class EditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxHeatFlares = new System.Windows.Forms.CheckBox();
            this.textBoxWeapon = new System.Windows.Forms.TextBox();
            this.textBoxWingspan = new System.Windows.Forms.TextBox();
            this.textBoxAttitude = new System.Windows.Forms.TextBox();
            this.textBoxDoors = new System.Windows.Forms.TextBox();
            this.textBoxCapacity = new System.Windows.Forms.TextBox();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelClass = new System.Windows.Forms.Label();
            this.labelWeapon = new System.Windows.Forms.Label();
            this.labelWingspan = new System.Windows.Forms.Label();
            this.labelAttitude = new System.Windows.Forms.Label();
            this.labelDoors = new System.Windows.Forms.Label();
            this.labelCapacity = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // checkBoxHeatFlares
            // 
            this.checkBoxHeatFlares.AutoSize = true;
            this.checkBoxHeatFlares.Location = new System.Drawing.Point(143, 277);
            this.checkBoxHeatFlares.Name = "checkBoxHeatFlares";
            this.checkBoxHeatFlares.Size = new System.Drawing.Size(90, 21);
            this.checkBoxHeatFlares.TabIndex = 35;
            this.checkBoxHeatFlares.Text = "Вспышки";
            this.checkBoxHeatFlares.UseVisualStyleBackColor = true;
            // 
            // textBoxWeapon
            // 
            this.textBoxWeapon.Location = new System.Drawing.Point(216, 240);
            this.textBoxWeapon.Name = "textBoxWeapon";
            this.textBoxWeapon.Size = new System.Drawing.Size(100, 22);
            this.textBoxWeapon.TabIndex = 34;
            // 
            // textBoxWingspan
            // 
            this.textBoxWingspan.Location = new System.Drawing.Point(216, 204);
            this.textBoxWingspan.MaxLength = 10;
            this.textBoxWingspan.Name = "textBoxWingspan";
            this.textBoxWingspan.Size = new System.Drawing.Size(100, 22);
            this.textBoxWingspan.TabIndex = 32;
            this.textBoxWingspan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // textBoxAttitude
            // 
            this.textBoxAttitude.Location = new System.Drawing.Point(216, 165);
            this.textBoxAttitude.MaxLength = 10;
            this.textBoxAttitude.Name = "textBoxAttitude";
            this.textBoxAttitude.Size = new System.Drawing.Size(100, 22);
            this.textBoxAttitude.TabIndex = 30;
            this.textBoxAttitude.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // textBoxDoors
            // 
            this.textBoxDoors.Location = new System.Drawing.Point(216, 165);
            this.textBoxDoors.MaxLength = 10;
            this.textBoxDoors.Name = "textBoxDoors";
            this.textBoxDoors.Size = new System.Drawing.Size(100, 22);
            this.textBoxDoors.TabIndex = 28;
            // 
            // textBoxCapacity
            // 
            this.textBoxCapacity.Location = new System.Drawing.Point(216, 128);
            this.textBoxCapacity.MaxLength = 10;
            this.textBoxCapacity.Name = "textBoxCapacity";
            this.textBoxCapacity.Size = new System.Drawing.Size(100, 22);
            this.textBoxCapacity.TabIndex = 26;
            this.textBoxCapacity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(216, 89);
            this.textBoxPrice.MaxLength = 10;
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(100, 22);
            this.textBoxPrice.TabIndex = 24;
            this.textBoxPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(216, 48);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 22);
            this.textBoxName.TabIndex = 22;
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(202, 338);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(90, 23);
            this.buttonClose.TabIndex = 19;
            this.buttonClose.Text = "Закрыть";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(94, 338);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 18;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // labelClass
            // 
            this.labelClass.AutoSize = true;
            this.labelClass.Location = new System.Drawing.Point(219, 9);
            this.labelClass.Name = "labelClass";
            this.labelClass.Size = new System.Drawing.Size(33, 17);
            this.labelClass.TabIndex = 36;
            this.labelClass.Text = "Тип";
            // 
            // labelWeapon
            // 
            this.labelWeapon.AutoSize = true;
            this.labelWeapon.Location = new System.Drawing.Point(10, 240);
            this.labelWeapon.Name = "labelWeapon";
            this.labelWeapon.Size = new System.Drawing.Size(59, 17);
            this.labelWeapon.TabIndex = 43;
            this.labelWeapon.Text = "Оружие";
            // 
            // labelWingspan
            // 
            this.labelWingspan.AutoSize = true;
            this.labelWingspan.Location = new System.Drawing.Point(10, 204);
            this.labelWingspan.Name = "labelWingspan";
            this.labelWingspan.Size = new System.Drawing.Size(141, 17);
            this.labelWingspan.TabIndex = 42;
            this.labelWingspan.Text = "Ширина крыльев (м)";
            // 
            // labelAttitude
            // 
            this.labelAttitude.AutoSize = true;
            this.labelAttitude.Location = new System.Drawing.Point(10, 167);
            this.labelAttitude.Name = "labelAttitude";
            this.labelAttitude.Size = new System.Drawing.Size(167, 17);
            this.labelAttitude.TabIndex = 41;
            this.labelAttitude.Text = "Высота полета  (тыс. м)";
            // 
            // labelDoors
            // 
            this.labelDoors.AutoSize = true;
            this.labelDoors.Location = new System.Drawing.Point(10, 165);
            this.labelDoors.Name = "labelDoors";
            this.labelDoors.Size = new System.Drawing.Size(137, 17);
            this.labelDoors.TabIndex = 40;
            this.labelDoors.Text = "Количество дверей";
            // 
            // labelCapacity
            // 
            this.labelCapacity.AutoSize = true;
            this.labelCapacity.Location = new System.Drawing.Point(10, 128);
            this.labelCapacity.Name = "labelCapacity";
            this.labelCapacity.Size = new System.Drawing.Size(162, 17);
            this.labelCapacity.TabIndex = 39;
            this.labelCapacity.Text = "Вместимость (человек)";
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(10, 89);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(65, 17);
            this.labelPrice.TabIndex = 38;
            this.labelPrice.Text = "Цена ($)";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(10, 48);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(72, 17);
            this.labelName.TabIndex = 37;
            this.labelName.Text = "Название";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 367);
            this.Controls.Add(this.labelWeapon);
            this.Controls.Add(this.labelWingspan);
            this.Controls.Add(this.labelAttitude);
            this.Controls.Add(this.labelDoors);
            this.Controls.Add(this.labelCapacity);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelClass);
            this.Controls.Add(this.checkBoxHeatFlares);
            this.Controls.Add(this.textBoxWeapon);
            this.Controls.Add(this.textBoxWingspan);
            this.Controls.Add(this.textBoxAttitude);
            this.Controls.Add(this.textBoxDoors);
            this.Controls.Add(this.textBoxCapacity);
            this.Controls.Add(this.textBoxPrice);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EditForm";
            this.Text = "Изменить";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxHeatFlares;
        private System.Windows.Forms.TextBox textBoxWeapon;
        private System.Windows.Forms.TextBox textBoxWingspan;
        private System.Windows.Forms.TextBox textBoxAttitude;
        private System.Windows.Forms.TextBox textBoxDoors;
        private System.Windows.Forms.TextBox textBoxCapacity;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelClass;
        private System.Windows.Forms.Label labelWeapon;
        private System.Windows.Forms.Label labelWingspan;
        private System.Windows.Forms.Label labelAttitude;
        private System.Windows.Forms.Label labelDoors;
        private System.Windows.Forms.Label labelCapacity;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.Label labelName;
    }
}