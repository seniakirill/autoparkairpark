﻿using System;
using System.Windows.Forms;
using LibAircompany;

namespace lab5
{
    public partial class EditForm : Form
    {
        public byte typeClass;
        public EditForm()
        {
            InitializeComponent();
            //MainForm main = this.Owner as MainForm;
        }
        public EditForm(AbstractVehicle veh)
        {
            InitializeComponent();
            Type type = veh.GetType();
            if (type.Equals(typeof(Bus)))
            {
                IntBus(veh);
                typeClass = 0;
            }
            else if (type.Equals(typeof(Jet)))
            {
                IntJet(veh);
                typeClass = 1;
            }
            else if (type.Equals(typeof(WarPlane)))
            {
                IntWarPlane(veh);
                typeClass = 2;
            }
        }
        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (typeClass == 0) EditBus();
            else if (typeClass == 1) EditJet();
            else if (typeClass == 2) EditWarPlane();
          //MainForm.listboxClass.
        }
        private void IntBus(AbstractVehicle veh)
        {
            labelClass.Text = "Автобус";

            labelAttitude.Visible = false;
            labelWingspan.Visible = false;
            labelWeapon.Visible = false;
            textBoxAttitude.Visible = false;
            textBoxWingspan.Visible = false;
            textBoxWeapon.Visible = false;
            checkBoxHeatFlares.Visible = false;

            Bus tr = (Bus)veh;
            textBoxName.Text = tr.Name;
            textBoxCapacity.Text = Convert.ToString(tr.Capacity);
            textBoxPrice.Text = Convert.ToString(tr.Price);
            textBoxDoors.Text = Convert.ToString(tr.Doors);
        }
        private void IntJet(AbstractVehicle veh)
        {
            labelClass.Text = "Самолет";
            textBoxWeapon.Visible = false;
            labelWeapon.Visible = false;
            textBoxDoors.Visible = false;
            labelDoors.Visible = false;
            checkBoxHeatFlares.Visible = false;

            Jet jt = (Jet)veh;
            textBoxName.Text = jt.Name;
            textBoxCapacity.Text = Convert.ToString(jt.Capacity);
            textBoxPrice.Text = Convert.ToString(jt.Price);
            textBoxAttitude.Text = Convert.ToString(jt.Attitude);
            textBoxWingspan.Text = Convert.ToString(jt.Wingspan);
        }

        private void IntWarPlane(AbstractVehicle veh)
        {
            labelClass.Text = "Истребитель"; 
            textBoxDoors.Visible = false;
            labelDoors.Visible = false;

            WarPlane warjt = (WarPlane)veh;
            textBoxName.Text = warjt.Name;
            textBoxCapacity.Text = Convert.ToString(warjt.Capacity);
            textBoxPrice.Text = Convert.ToString(warjt.Price);
            textBoxAttitude.Text = Convert.ToString(warjt.Attitude);
            textBoxWingspan.Text = Convert.ToString(warjt.Wingspan);
            textBoxWeapon.Text = warjt.Weapon;
            checkBoxHeatFlares.Checked = warjt.HeatFlares;
        }
        private void EditBus()
        {
            Bus newBus = new Bus();
            newBus.Name = textBoxName.Text;
            newBus.Price = Convert.ToInt32(textBoxPrice.Text);
            newBus.Capacity = Convert.ToInt32(textBoxCapacity.Text);
            newBus.Doors = Convert.ToInt32(textBoxDoors.Text);
            Close();
            MainForm.SelfRef.EditAutopark(newBus);
        }
        private void EditJet()
        {
            Jet newJet = new Jet();
            newJet.Name = textBoxName.Text;
            newJet.Price = Convert.ToInt32(textBoxPrice.Text);
            newJet.Capacity = Convert.ToInt32(textBoxCapacity.Text);
            newJet.Attitude = Convert.ToInt32(textBoxAttitude.Text);
            newJet.Wingspan = Convert.ToInt32(textBoxWingspan.Text);
            Close();
            MainForm.SelfRef.EditAutopark(newJet);
        }
        private void EditWarPlane()
        {
            WarPlane newWarPlane = new WarPlane();
            newWarPlane.Name = textBoxName.Text;
            newWarPlane.Price = Convert.ToInt32(textBoxPrice.Text);
            newWarPlane.Capacity = Convert.ToInt32(textBoxCapacity.Text);
            newWarPlane.Attitude = Convert.ToInt32(textBoxAttitude.Text);
            newWarPlane.Wingspan = Convert.ToInt32(textBoxWingspan.Text);
            newWarPlane.Weapon = textBoxWeapon.Text;
            if (newWarPlane.HeatFlares == true)
                checkBoxHeatFlares.Checked = true;
            else checkBoxHeatFlares.Checked = false;
            Close();
            MainForm.SelfRef.EditAutopark(newWarPlane);
        }

        private void textBoxPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
                if (Char.IsNumber(e.KeyChar) | (e.KeyChar == Convert.ToChar(",")) | e.KeyChar == '\b') return;
                else
                    e.Handled = true;
        }
    }
}