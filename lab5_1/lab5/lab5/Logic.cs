﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using LibAircompany;
namespace lab5
{
    static class Logic
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        public class AirFactory
        {
            public static Park CreatePark()
            {
                //Создание объектов
                Park park = new Park();

                Bus buscar = new Bus();
                buscar.Name = "МАЗ-171";
                buscar.Capacity = 100;
                buscar.Price = 30000;
                buscar.Doors = 2;
                park.AddVeh(buscar);

                Jet jt = new Jet();
                jt.Capacity = 300;
                jt.Price = 10000000;
                jt.Name = "Boeing 777";
                jt.Price = 1000000;
                jt.Attitude = 8;
                jt.Wingspan = 25;
                park.AddVeh(jt);

                WarPlane warjt = new WarPlane();
                warjt.Capacity = 1;
                warjt.Price = 50000000;
                warjt.Name = "Su-27";
                warjt.Attitude = 12;
                warjt.Wingspan = 10;
                warjt.Weapon = "Pulemet";
                warjt.HeatFlares = true;

                park.AddVeh(warjt);

                return park;
            }
        }
        public class AirCalculator
        {
            public static double GetTotalCapacity(Park park)
            {
                double TotalCapacity = 0;
                List<AbstractVehicle> cars = park.vehicle;
                int count = cars.Count();
                for (int i = 0; i < count; i++)
                    TotalCapacity += cars[i].Capacity;
                return TotalCapacity;
            }
        }
    }
}
