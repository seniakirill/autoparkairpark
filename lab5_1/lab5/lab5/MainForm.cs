﻿using System;
using System.Linq;
using System.Windows.Forms;
using LibAircompany;
using static lab5.Logic;

namespace lab5
{
    public partial class MainForm : Form
    {
        public static MainForm SelfRef { get; set; }
        public Park vehpark;
        public MainForm()
        {
            SelfRef = this;
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            vehpark = AirFactory.CreatePark();
            ReCalc();
           // for (int i = 0; i < vehpark.vehicle.Count(); i++)
             //   listBoxClass.Items.Add(vehpark.vehicle[i].Name+"  мест: "+vehpark.vehicle[i].Capacity);
        }
        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddForm newFormAdd = new AddForm();
           // Owner = this;
            newFormAdd.ShowDialog();
            buttonEdit.Enabled = false;
            buttonDelete.Enabled = false;
        }
        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (listBoxClass.SelectedIndex != -1)
            {
                int ind = listBoxClass.SelectedIndex;
                EditForm newFormEdit = new EditForm(vehpark.vehicle[ind]);
               // Owner = this;
                newFormEdit.ShowDialog();
                listBoxClass.ClearSelected();
                buttonEdit.Enabled = false;
                buttonDelete.Enabled = false;
            }
        }
        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (listBoxClass.SelectedIndex != -1)
            {
                int i = listBoxClass.SelectedIndex;
                listBoxClass.Items.RemoveAt(i);
                vehpark.vehicle.RemoveAt(i);
            }
            ReCalc();
            buttonEdit.Enabled = false;
            buttonDelete.Enabled = false;
        }
        public void EditAutopark(AbstractVehicle veh)
        {
            int ind = listBoxClass.SelectedIndex;
            vehpark.vehicle[ind] = veh;
            listBoxClass.Items.RemoveAt(ind);
            listBoxClass.Items.Insert(ind, veh.Name);
            ReCalc();
            
        }
        private void ReCalc()
        {
            //Подсчет стоимости автопарка
            labelSum.Text = Convert.ToString("Общая вместимость " + AirCalculator.GetTotalCapacity(vehpark)) + " человек";
            listBoxClass.Items.Clear();
            for (int i = 0; i < vehpark.vehicle.Count(); i++)
                listBoxClass.Items.Add(vehpark.vehicle[i].Name + "  мест: " + vehpark.vehicle[i].Capacity);
        }
        public void AddCarInAutopark(AbstractVehicle veh)
        {
            listBoxClass.Items.Add(veh.Name);
            vehpark.AddVeh(veh);
            ReCalc();
        }

        private void listBoxClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonEdit.Enabled = true;
            buttonDelete.Enabled = true;
        }
    }
}
