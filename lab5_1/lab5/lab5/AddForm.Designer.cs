﻿namespace lab5
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.comboBoxClass = new System.Windows.Forms.ComboBox();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.labelPrice = new System.Windows.Forms.Label();
            this.textBoxCapacity = new System.Windows.Forms.TextBox();
            this.labelCapacity = new System.Windows.Forms.Label();
            this.textBoxDoors = new System.Windows.Forms.TextBox();
            this.labelDoors = new System.Windows.Forms.Label();
            this.textBoxAttitude = new System.Windows.Forms.TextBox();
            this.labelAttitude = new System.Windows.Forms.Label();
            this.textBoxWingspan = new System.Windows.Forms.TextBox();
            this.labelWingspan = new System.Windows.Forms.Label();
            this.textBoxWeapon = new System.Windows.Forms.TextBox();
            this.labelWeapon = new System.Windows.Forms.Label();
            this.checkBoxHeatFlares = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(84, 331);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(192, 331);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(85, 23);
            this.buttonClose.TabIndex = 1;
            this.buttonClose.Text = "Закрыть";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // comboBoxClass
            // 
            this.comboBoxClass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxClass.FormattingEnabled = true;
            this.comboBoxClass.Location = new System.Drawing.Point(241, 11);
            this.comboBoxClass.Name = "comboBoxClass";
            this.comboBoxClass.Size = new System.Drawing.Size(121, 24);
            this.comboBoxClass.TabIndex = 2;
            this.comboBoxClass.SelectedIndexChanged += new System.EventHandler(this.comboBoxClass_SelectedIndexChanged);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(12, 54);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(72, 17);
            this.labelName.TabIndex = 3;
            this.labelName.Text = "Название";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(241, 51);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 22);
            this.textBoxName.TabIndex = 4;
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(241, 92);
            this.textBoxPrice.MaxLength = 10;
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(100, 22);
            this.textBoxPrice.TabIndex = 6;
            this.textBoxPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(12, 95);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(65, 17);
            this.labelPrice.TabIndex = 5;
            this.labelPrice.Text = "Цена ($)";
            // 
            // textBoxCapacity
            // 
            this.textBoxCapacity.Location = new System.Drawing.Point(241, 131);
            this.textBoxCapacity.MaxLength = 10;
            this.textBoxCapacity.Name = "textBoxCapacity";
            this.textBoxCapacity.Size = new System.Drawing.Size(100, 22);
            this.textBoxCapacity.TabIndex = 8;
            this.textBoxCapacity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // labelCapacity
            // 
            this.labelCapacity.AutoSize = true;
            this.labelCapacity.Location = new System.Drawing.Point(12, 134);
            this.labelCapacity.Name = "labelCapacity";
            this.labelCapacity.Size = new System.Drawing.Size(162, 17);
            this.labelCapacity.TabIndex = 7;
            this.labelCapacity.Text = "Вместимость (человек)";
            // 
            // textBoxDoors
            // 
            this.textBoxDoors.Location = new System.Drawing.Point(241, 173);
            this.textBoxDoors.Name = "textBoxDoors";
            this.textBoxDoors.Size = new System.Drawing.Size(100, 22);
            this.textBoxDoors.TabIndex = 10;
            this.textBoxDoors.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // labelDoors
            // 
            this.labelDoors.AutoSize = true;
            this.labelDoors.Location = new System.Drawing.Point(12, 171);
            this.labelDoors.Name = "labelDoors";
            this.labelDoors.Size = new System.Drawing.Size(137, 17);
            this.labelDoors.TabIndex = 9;
            this.labelDoors.Text = "Количество дверей";
            // 
            // textBoxAttitude
            // 
            this.textBoxAttitude.Location = new System.Drawing.Point(241, 173);
            this.textBoxAttitude.MaxLength = 10;
            this.textBoxAttitude.Name = "textBoxAttitude";
            this.textBoxAttitude.Size = new System.Drawing.Size(100, 22);
            this.textBoxAttitude.TabIndex = 12;
            this.textBoxAttitude.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // labelAttitude
            // 
            this.labelAttitude.AutoSize = true;
            this.labelAttitude.Location = new System.Drawing.Point(12, 176);
            this.labelAttitude.Name = "labelAttitude";
            this.labelAttitude.Size = new System.Drawing.Size(167, 17);
            this.labelAttitude.TabIndex = 11;
            this.labelAttitude.Text = "Высота полета  (тыс. м)";
            // 
            // textBoxWingspan
            // 
            this.textBoxWingspan.Location = new System.Drawing.Point(241, 207);
            this.textBoxWingspan.MaxLength = 10;
            this.textBoxWingspan.Name = "textBoxWingspan";
            this.textBoxWingspan.Size = new System.Drawing.Size(100, 22);
            this.textBoxWingspan.TabIndex = 14;
            this.textBoxWingspan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // labelWingspan
            // 
            this.labelWingspan.AutoSize = true;
            this.labelWingspan.Location = new System.Drawing.Point(12, 210);
            this.labelWingspan.Name = "labelWingspan";
            this.labelWingspan.Size = new System.Drawing.Size(141, 17);
            this.labelWingspan.TabIndex = 13;
            this.labelWingspan.Text = "Ширина крыльев (м)";
            // 
            // textBoxWeapon
            // 
            this.textBoxWeapon.Location = new System.Drawing.Point(241, 243);
            this.textBoxWeapon.Name = "textBoxWeapon";
            this.textBoxWeapon.Size = new System.Drawing.Size(100, 22);
            this.textBoxWeapon.TabIndex = 16;
            // 
            // labelWeapon
            // 
            this.labelWeapon.AutoSize = true;
            this.labelWeapon.Location = new System.Drawing.Point(12, 246);
            this.labelWeapon.Name = "labelWeapon";
            this.labelWeapon.Size = new System.Drawing.Size(59, 17);
            this.labelWeapon.TabIndex = 15;
            this.labelWeapon.Text = "Оружие";
            // 
            // checkBoxHeatFlares
            // 
            this.checkBoxHeatFlares.AutoSize = true;
            this.checkBoxHeatFlares.Location = new System.Drawing.Point(127, 285);
            this.checkBoxHeatFlares.Name = "checkBoxHeatFlares";
            this.checkBoxHeatFlares.Size = new System.Drawing.Size(90, 21);
            this.checkBoxHeatFlares.TabIndex = 17;
            this.checkBoxHeatFlares.Text = "Вспышки";
            this.checkBoxHeatFlares.UseVisualStyleBackColor = true;
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 368);
            this.Controls.Add(this.checkBoxHeatFlares);
            this.Controls.Add(this.textBoxWeapon);
            this.Controls.Add(this.labelWeapon);
            this.Controls.Add(this.textBoxWingspan);
            this.Controls.Add(this.labelWingspan);
            this.Controls.Add(this.textBoxAttitude);
            this.Controls.Add(this.labelAttitude);
            this.Controls.Add(this.textBoxDoors);
            this.Controls.Add(this.labelDoors);
            this.Controls.Add(this.textBoxCapacity);
            this.Controls.Add(this.labelCapacity);
            this.Controls.Add(this.textBoxPrice);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.comboBoxClass);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddForm";
            this.Text = "Добавить";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.ComboBox comboBoxClass;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.TextBox textBoxCapacity;
        private System.Windows.Forms.Label labelCapacity;
        private System.Windows.Forms.TextBox textBoxDoors;
        private System.Windows.Forms.Label labelDoors;
        private System.Windows.Forms.TextBox textBoxAttitude;
        private System.Windows.Forms.Label labelAttitude;
        private System.Windows.Forms.TextBox textBoxWingspan;
        private System.Windows.Forms.Label labelWingspan;
        private System.Windows.Forms.TextBox textBoxWeapon;
        private System.Windows.Forms.Label labelWeapon;
        private System.Windows.Forms.CheckBox checkBoxHeatFlares;
    }
}