﻿using System;
using System.Windows.Forms;
using LibAircompany;

namespace lab5
{
    public partial class AddForm : Form
    {
        public AddForm()
        {
            InitializeComponent();
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            ComboBoxCreate();
        }
        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {

                int ind = comboBoxClass.SelectedIndex;
                if (ind == 0) CreateBus();
                else if (ind == 1) CreateJet();
                else if (ind == 2) CreateWarPlane();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Некорректный ввод!");
                Console.WriteLine(ex.Message);
                Close();

            }
            // MainForm.
        }
        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void ComboBoxCreate()
        {
            comboBoxClass.Items.Add("Bus");
            comboBoxClass.Items.Add("Jet");
            comboBoxClass.Items.Add("Warplane");
            comboBoxClass.SelectedIndex = 0;
            //AddBus();
        }
             private void CreateBus()
        {
            Bus newBus = new Bus();
            newBus.Name = textBoxName.Text;
            newBus.Price = Convert.ToInt32(textBoxPrice.Text);
            newBus.Capacity = Convert.ToInt32(textBoxCapacity.Text);
            newBus.Doors = Convert.ToInt32(textBoxDoors.Text);
            Close();
            MainForm.SelfRef.AddCarInAutopark(newBus);
        }
        private void CreateJet()
        {
            Jet newJet = new Jet();
            newJet.Name = textBoxName.Text;
            newJet.Price = Convert.ToInt32(textBoxPrice.Text);
            newJet.Capacity = Convert.ToInt32(textBoxCapacity.Text);
            newJet.Attitude = Convert.ToInt32(textBoxAttitude.Text);
            newJet.Wingspan = Convert.ToInt32(textBoxWingspan.Text);
            Close();
            MainForm.SelfRef.AddCarInAutopark(newJet);
        }
        private void CreateWarPlane()
        {
            WarPlane newWarPlane = new WarPlane();
            newWarPlane.Name = textBoxName.Text;
            newWarPlane.Price = Convert.ToInt32(textBoxPrice.Text);
            newWarPlane.Capacity = Convert.ToInt32(textBoxCapacity.Text);
            newWarPlane.Attitude = Convert.ToInt32(textBoxAttitude.Text);
            newWarPlane.Wingspan = Convert.ToInt32(textBoxWingspan.Text);
            newWarPlane.Weapon = textBoxWeapon.Text;
            if (checkBoxHeatFlares.Checked == true)
                newWarPlane.HeatFlares = true;
            else newWarPlane.HeatFlares = false;
            Close();
            MainForm.SelfRef.AddCarInAutopark(newWarPlane);
        }

        private void comboBoxClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxClass.SelectedIndex == 0)
            {
                textBoxAttitude.Visible = false;
                textBoxWingspan.Visible = false;
                textBoxWeapon.Visible = false;
                labelAttitude.Visible = false;
                labelWingspan.Visible = false;
                labelWeapon.Visible = false;
                checkBoxHeatFlares.Visible = false;
                labelDoors.Visible = true;
                textBoxDoors.Visible = true;
            }
            if (comboBoxClass.SelectedIndex == 1)
            {
                labelAttitude.Visible = true;
                labelWingspan.Visible = true;
                textBoxAttitude.Visible = true;
                textBoxWingspan.Visible = true;
                labelWeapon.Visible = false;
                textBoxWeapon.Visible = false;
                checkBoxHeatFlares.Visible = false;
                labelDoors.Visible = false;
                textBoxDoors.Visible = false;
                checkBoxHeatFlares.Visible = false;
            }
            if (comboBoxClass.SelectedIndex == 2)
            {
                labelAttitude.Visible = true;
                labelWingspan.Visible = true;
                labelWeapon.Visible = true;
                textBoxAttitude.Visible = true;
                textBoxWingspan.Visible = true;
                textBoxWeapon.Visible = true;
                checkBoxHeatFlares.Visible = true;
                labelDoors.Visible = false;
                textBoxDoors.Visible = false;
                checkBoxHeatFlares.Visible = true;
            }
        }
        private void textBoxPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) | (e.KeyChar == Convert.ToChar(",")) | e.KeyChar == '\b') return;
            else
                e.Handled = true;
        }
    }
}