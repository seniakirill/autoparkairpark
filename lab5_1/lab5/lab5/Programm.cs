﻿using System;
using System.Collections.Generic;
using System.Linq;
using LibAircompany;
namespace lab4_thi
{
    class Program
    {
        static void notMain(string[] args)
        {
            Park park = AirFactory.CreatePark();
            AirPrinter.Print(park);
            double capacity = AirCalculator.GetTotalCapacity(park);
            //Console.WriteLine("Total Capacity = " + capacity);
            //Console.ReadLine();
        }
        public class AirPrinter
        {
            public static void Print(Park park)
            {
                List<AbstractVehicle> cars = park.vehicle;
              //  Console.WriteLine("Capacity:");
                int count = cars.Count();
                for (int i = 0; i < count; i++)
                {
                    //Console.Write("Name = " + cars[i].Name);
                    //Console.WriteLine("  Capacity = " + cars[i].Capacity);
                }
            }
        }
        public class AirFactory
        {
            public static Park CreatePark()
            {
                //Создание объектов
                Park park = new Park();

                Bus buscar = new Bus();
                buscar.Name = "МАЗ-171";
                buscar.Capacity = 100;
                buscar.Price = 30000;
                buscar.Doors = 2;
                park.AddVeh(buscar);

                Jet jt = new Jet();
                jt.Capacity = 300;
                jt.Price = 10000000;
                jt.Name = "Boeing 777";
                jt.Price = 1000000;
                jt.Attitude = 8;
                jt.Wingspan = 25;
                park.AddVeh(jt);

                WarPlane warjt = new WarPlane();
                warjt.Capacity = 1;
                warjt.Price = 50000000;
                warjt.Name = "Su-27";
                warjt.Attitude = 12;
                warjt.Wingspan = 10;
                warjt.Weapon = "Pulemet";
                warjt.HeatFlares = true;
                
                park.AddVeh(warjt);
                
                return park;
            }
        }
        public class AirCalculator
        {
            public static double GetTotalCapacity(Park park)
            {
                double TotalCapacity = 0;
                List<AbstractVehicle> cars = park.vehicle;
                int count = cars.Count();
                for (int i = 0; i < count; i++)
                    TotalCapacity += cars[i].Capacity;
               return TotalCapacity;
            }
        }
    }
}