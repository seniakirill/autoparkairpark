﻿using LabTHI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class PriceCalculator
    {
        public int GetSum(AutoPark auto  )
        {
            int sum = 0;
            foreach (AbstractCar car in auto.Cars)
            {
                sum = sum + car.Price;
            }
            return sum;
        }
    }
}
