﻿using LabTHI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class AutoParkFactory
    {
        public static AutoPark CreateAutoPark()
        {

        AutoPark auto = new AutoPark();
            auto.Name = "AutoPark";
            
            CloseCarBody ChevroletCamaro = new CloseCarBody("Chevrolet Camaro", 2990000, 1.539, 238,false, "Машины с закрытым корпусом");
            ArmoredVehicles ToyotaLandCruiser = new ArmoredVehicles("Toyota Land Cruiser 200 B6",9500000,2.715,308,false,3, "Бронированные автомобили");
            auto.AddCar(ChevroletCamaro);
            auto.AddCar(ToyotaLandCruiser);
            return auto;
        }
    }
}
