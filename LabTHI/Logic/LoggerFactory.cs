﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class LoggerFactory
    {
        public ILogger CreateLogger(int type)
        {
            if (type.Equals("console"))
                return new ConsoleLogger();
            else
                return new FIleLoggeer();
        }
    }
}
