﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabTHI
{
   public class ArmoredVehicles : CloseCarBody
    {

        int _armorThickness;
        public ArmoredVehicles(string _model, int _price, double _weight, int _power, bool _septum, int _armorThickness,string _class) : base(_model, _price, _weight, _power, _septum, _class)
        {
            this._armorThickness = _armorThickness;

        }
        public int ArmorThickness
        {
            get { return _armorThickness; }
            set { _armorThickness = value; }
        }
        
    }
}
