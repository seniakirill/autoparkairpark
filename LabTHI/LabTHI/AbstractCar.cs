﻿using System;

namespace LabTHI
{
    public abstract class AbstractCar
    {
        int _price;
        double _weight;
        int _power;
        string _model;
        string _class;
       


        public AbstractCar(string _model,int _price, double _weight, int _power,string _class )
        {
            this._model = _model;
            this._price = _price;
            this._weight = _weight;
            this._power = _power;
            this._class = _class;
         
        }

        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public int Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public int Power
        {
            get { return _power; }
            set { _power = value; }
        }
        public string Class
        {
            get { return _class; }
            set { _class = value; }
        }


    }
}
