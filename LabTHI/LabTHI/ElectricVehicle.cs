﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabTHI
{
  public  class ElectricVehicle : CloseCarBody
    {
        int _electricalCapacitance;
        public ElectricVehicle(string _model, int _price, double _weight, int _power, bool _septum, int _electricalCapacitance, string _class) : base(_model, _price, _weight, _power, _septum, _class)
        {
            this._electricalCapacitance = _electricalCapacitance;
        }
        public int ElectricalCapacitance
        {
            get { return _electricalCapacitance; }
            set { _electricalCapacitance = value; }
        }
    }
}
