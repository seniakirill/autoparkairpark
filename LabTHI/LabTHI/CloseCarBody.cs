﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabTHI
{
   public class CloseCarBody : AbstractCar
    {


        bool _septum;
        public CloseCarBody(string _model, int _price, double _weight, int _power , bool _septum, string _class) : base(_model, _price,_weight,_power,_class) 
        {
          
            this._septum = _septum;
        }
        public bool Septum
        {
            get { return _septum; }
            set { _septum = value; }
        }
    }
}
