﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LabTHI
{
  public  class OpenCarBody : AbstractCar
    {
        string _typeRoof;
        public OpenCarBody(string _model, int _price, double _weight, int _power, string _typeRoof, string _class) : base(_model, _price, _weight, _power,_class)
        {
  
        }
        public string TypeRoof
        {
            get { return _typeRoof; }
            set { _typeRoof = value; }
        }
    }
}
