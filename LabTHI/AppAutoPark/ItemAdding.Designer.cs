﻿namespace AppAutoPark
{
    partial class ItemAdding
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.classCB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Model = new System.Windows.Forms.TextBox();
            this.Price = new System.Windows.Forms.TextBox();
            this.Power = new System.Windows.Forms.TextBox();
            this.weight = new System.Windows.Forms.TextBox();
            this.energy = new System.Windows.Forms.TextBox();
            this.armor = new System.Windows.Forms.TextBox();
            this.typeroof = new System.Windows.Forms.TextBox();
            this.yes = new System.Windows.Forms.RadioButton();
            this.no = new System.Windows.Forms.RadioButton();
            this.ItemAddingButton = new System.Windows.Forms.Button();
            this.CancelAddingButton = new System.Windows.Forms.Button();
            this.EnterNumber = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // classCB
            // 
            this.classCB.FormattingEnabled = true;
            this.classCB.Location = new System.Drawing.Point(304, 44);
            this.classCB.Name = "classCB";
            this.classCB.Size = new System.Drawing.Size(247, 21);
            this.classCB.TabIndex = 0;
            this.classCB.Text = "Выберите класс элемента";
            this.classCB.SelectedIndexChanged += new System.EventHandler(this.classCB_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(260, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Класс";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Модель";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Цена(руб)";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Мощность(л.с.)";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(47, 238);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Вес(т)";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(420, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Энергоемкость";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(420, 166);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Уровень Защиты";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(420, 202);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Тип Крыши";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(420, 238);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Наличие перегородки";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // Model
            // 
            this.Model.Location = new System.Drawing.Point(156, 122);
            this.Model.Name = "Model";
            this.Model.Size = new System.Drawing.Size(100, 20);
            this.Model.TabIndex = 10;
            this.Model.TextChanged += new System.EventHandler(this.Model_TextChanged);
            // 
            // Price
            // 
            this.Price.Location = new System.Drawing.Point(154, 163);
            this.Price.Name = "Price";
            this.Price.Size = new System.Drawing.Size(102, 20);
            this.Price.TabIndex = 11;
            this.Price.TextChanged += new System.EventHandler(this.Price_TextChanged);
            // 
            // Power
            // 
            this.Power.Location = new System.Drawing.Point(154, 199);
            this.Power.Name = "Power";
            this.Power.Size = new System.Drawing.Size(102, 20);
            this.Power.TabIndex = 12;
            this.Power.TextChanged += new System.EventHandler(this.Power_TextChanged);
            // 
            // weight
            // 
            this.weight.Location = new System.Drawing.Point(154, 235);
            this.weight.Name = "weight";
            this.weight.Size = new System.Drawing.Size(102, 20);
            this.weight.TabIndex = 13;
            this.weight.TextChanged += new System.EventHandler(this.weight_TextChanged);
            // 
            // energy
            // 
            this.energy.Location = new System.Drawing.Point(582, 122);
            this.energy.Name = "energy";
            this.energy.Size = new System.Drawing.Size(89, 20);
            this.energy.TabIndex = 14;
            this.energy.TextChanged += new System.EventHandler(this.energy_TextChanged);
            // 
            // armor
            // 
            this.armor.Location = new System.Drawing.Point(582, 163);
            this.armor.Name = "armor";
            this.armor.Size = new System.Drawing.Size(89, 20);
            this.armor.TabIndex = 15;
            this.armor.TextChanged += new System.EventHandler(this.armor_TextChanged);
            // 
            // typeroof
            // 
            this.typeroof.Location = new System.Drawing.Point(582, 199);
            this.typeroof.Name = "typeroof";
            this.typeroof.Size = new System.Drawing.Size(89, 20);
            this.typeroof.TabIndex = 16;
            this.typeroof.TextChanged += new System.EventHandler(this.typeroof_TextChanged);
            // 
            // yes
            // 
            this.yes.AutoSize = true;
            this.yes.Location = new System.Drawing.Point(581, 234);
            this.yes.Name = "yes";
            this.yes.Size = new System.Drawing.Size(40, 17);
            this.yes.TabIndex = 17;
            this.yes.TabStop = true;
            this.yes.Text = "Да";
            this.yes.UseVisualStyleBackColor = true;
            this.yes.CheckedChanged += new System.EventHandler(this.yes_CheckedChanged);
            // 
            // no
            // 
            this.no.AutoSize = true;
            this.no.Location = new System.Drawing.Point(627, 234);
            this.no.Name = "no";
            this.no.Size = new System.Drawing.Size(44, 17);
            this.no.TabIndex = 18;
            this.no.TabStop = true;
            this.no.Text = "Нет";
            this.no.UseVisualStyleBackColor = true;
            this.no.CheckedChanged += new System.EventHandler(this.no_CheckedChanged);
            // 
            // ItemAddingButton
            // 
            this.ItemAddingButton.Location = new System.Drawing.Point(223, 356);
            this.ItemAddingButton.Name = "ItemAddingButton";
            this.ItemAddingButton.Size = new System.Drawing.Size(126, 27);
            this.ItemAddingButton.TabIndex = 19;
            this.ItemAddingButton.Text = "Добавить";
            this.ItemAddingButton.UseVisualStyleBackColor = true;
            this.ItemAddingButton.Click += new System.EventHandler(this.ItemAddingButton_Click);
            // 
            // CancelAddingButton
            // 
            this.CancelAddingButton.Location = new System.Drawing.Point(423, 356);
            this.CancelAddingButton.Name = "CancelAddingButton";
            this.CancelAddingButton.Size = new System.Drawing.Size(125, 27);
            this.CancelAddingButton.TabIndex = 20;
            this.CancelAddingButton.Text = "Отмена";
            this.CancelAddingButton.UseVisualStyleBackColor = true;
            this.CancelAddingButton.Click += new System.EventHandler(this.CancelAddingButton_Click);
            // 
            // ItemAdding
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.CancelAddingButton);
            this.Controls.Add(this.ItemAddingButton);
            this.Controls.Add(this.no);
            this.Controls.Add(this.yes);
            this.Controls.Add(this.typeroof);
            this.Controls.Add(this.armor);
            this.Controls.Add(this.energy);
            this.Controls.Add(this.weight);
            this.Controls.Add(this.Power);
            this.Controls.Add(this.Price);
            this.Controls.Add(this.Model);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.classCB);
            this.Name = "ItemAdding";
            this.Text = "ItemAdding";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox classCB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Model;
        private System.Windows.Forms.TextBox Price;
        private System.Windows.Forms.TextBox Power;
        private System.Windows.Forms.TextBox weight;
        private System.Windows.Forms.TextBox energy;
        private System.Windows.Forms.TextBox armor;
        private System.Windows.Forms.TextBox typeroof;
        private System.Windows.Forms.RadioButton yes;
        private System.Windows.Forms.RadioButton no;
        private System.Windows.Forms.Button ItemAddingButton;
        private System.Windows.Forms.Button CancelAddingButton;
        private System.Windows.Forms.ToolTip EnterNumber;
    }
}