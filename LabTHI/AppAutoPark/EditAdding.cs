﻿using LabTHI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppAutoPark
{
    public partial class EditAdding : Form
    {
        public  const string CloseCarBodyConst = "Машины с закрытым корпусом";
        public const string OpenCarBodyConst = "Машины с открытым корпусом";
        public const string ElectricVehicleConst = "Электромобили";
        public const string ArmoredVehicleConst = "Бронированные автомобили";
        // методы, которые вызываются при инициализации формы
        private void ChangeCar(AbstractCar car)
        {
            // общие поля
            Model.Text = car.Model;
            Price.Text = Convert.ToString(car.Price);
            weight.Text = Convert.ToString(car.Weight);
            Power.Text = Convert.ToString(car.Power);
            typeroof.Enabled = false;
            armor.Enabled = false;
            yes.Enabled = false;
            no.Enabled = false;
            energy.Enabled = false;
        }
        private void ChangeCloseCarBody(AbstractCar car)
        {
            // специфичные для автомобилей поля
            // делаем доступными
            classCB.SelectedItem = CloseCarBodyConst;
            classCB.Enabled = false;
            yes.Enabled = true;
            no.Enabled = true;
            // заполняем
            if (((CloseCarBody)car).Septum == true)
                yes.Checked = true;
            else no.Checked = true;
        }
        private void ChangeOpenCarBody(AbstractCar car)
        {
            // выпадающий список
            classCB.SelectedItem = OpenCarBodyConst;
            classCB.Enabled = false;

            typeroof.Enabled = true;
            typeroof.Text = Convert.ToString(((OpenCarBody)car).TypeRoof);

        }
        private void ChangeElectricVehicle(AbstractCar car)
        {

            classCB.SelectedItem = ElectricVehicleConst;
            classCB.Enabled = false;


            energy.Enabled = true;
            energy.Text = Convert.ToString(((ElectricVehicle)car).ElectricalCapacitance);
        }
        private void ChangeArmoredVehicles(AbstractCar car)
        {

            classCB.SelectedItem = ArmoredVehicleConst;
            classCB.Enabled = false;


            armor.Enabled = true;
            armor.Text = Convert.ToString(((ArmoredVehicles)car).ArmorThickness);
        }
        private AbstractCar _editedCar;
        public AbstractCar EditedCar
        {
            get { return _editedCar; }
            set { _editedCar = value; }
        }
        private int _selectedIndex;
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { _selectedIndex = value; }
        }
        private DialogResult _result;
        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        }
        // методы, которые вызываются при редактировании объекта
        private void ApplyCloseCarBody()
        {  
                EditedCar = new CloseCarBody(Model.Text, Convert.ToInt32(Price.Text), Convert.ToDouble(weight.Text), Convert.ToInt32(Power.Text), yes.Checked, CloseCarBodyConst);
            
 
        }
        private void ApplyOpenCarBody()
        {
            EditedCar = new OpenCarBody(Model.Text, Convert.ToInt32(Price.Text), Convert.ToDouble(weight.Text), Convert.ToInt32(Power.Text), typeroof.Text, OpenCarBodyConst);

        }
        private void ApplyArmoredVehicles()
        {
            EditedCar = new ArmoredVehicles(Model.Text, Convert.ToInt32(Price.Text), Convert.ToDouble(weight.Text), Convert.ToInt32(Power.Text), yes.Checked, Convert.ToInt32(armor.Text),ArmoredVehicleConst);

        }
        private void ApplyElectricVehicle()
        {
            EditedCar = new ElectricVehicle(Model.Text, Convert.ToInt32(Price.Text), Convert.ToDouble(weight.Text), Convert.ToInt32(Power.Text), yes.Checked, Convert.ToInt32(energy.Text),ElectricVehicleConst);

        }


        public EditAdding(AbstractCar car, int selectedIndex)
        {
            InitializeComponent();
            classCB.Items.AddRange(new String[] {CloseCarBodyConst,
                OpenCarBodyConst, ElectricVehicleConst,ArmoredVehicleConst});
            // Запоминаем номер выделенного элемента
            SelectedIndex = selectedIndex;

            ChangeCar(car);

            switch (car.Class)
            {
                case CloseCarBodyConst:
                    {
                        ChangeCloseCarBody(car);
                        break;
                    }
                case OpenCarBodyConst:
                    {
                        ChangeOpenCarBody(car);
                        break;
                    }
                case ElectricVehicleConst:
                    {
                        ChangeCloseCarBody(car);
                        ChangeElectricVehicle(car);
                        break;
                    }
                case ArmoredVehicleConst:
                    {
                        ChangeCloseCarBody(car);
                        ChangeArmoredVehicles(car);
                        break;
                    }
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            try
            {

                String selected = classCB.SelectedItem.ToString();
                switch (selected)
                {
                    case CloseCarBodyConst:
                        {
                            if ((Convert.ToInt32(Price.Text) >= 0) && (Convert.ToDouble(weight.Text) >= 0) && (Convert.ToInt32(Power.Text) >= 0))
                            {
                                ApplyCloseCarBody();
                                Result = DialogResult.OK;
                                Close();
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Отрицательные значения", "Ошибка");
                                break;
                            }
                        }
                    case OpenCarBodyConst:
                        {
                              if ((Convert.ToInt32(Price.Text) >= 0) && (Convert.ToDouble(weight.Text) >= 0) && (Convert.ToInt32(Power.Text) >= 0))
                            {
                                ApplyOpenCarBody();
                                Result = DialogResult.OK;
                                Close();
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Отрицательные значения", "Ошибка");
                                break;
                            }
                        }
                    case ElectricVehicleConst:
                        {
                            if ((Convert.ToInt32(energy.Text) >= 0) && (Convert.ToInt32(Price.Text) >= 0) && (Convert.ToDouble(weight.Text) >= 0) && (Convert.ToInt32(Power.Text) >= 0))
                            {
                                ApplyElectricVehicle();
                                Result = DialogResult.OK;
                                Close();
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Отрицательные значения", "Ошибка");
                                break;
                            }
                        }
                    case ArmoredVehicleConst:
                        {
                            if ((Convert.ToInt32(armor.Text) >= 0)&&(Convert.ToInt32(Price.Text)>=0)&&(Convert.ToDouble(weight.Text)>=0)&&(Convert.ToInt32(Power.Text)>=0))
                            {
                                ApplyArmoredVehicles();
                                Result = DialogResult.OK;
                                Close();
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Отрицательные значения", "Ошибка");
                                break;
                            }
                        }
                }
            
           
        }
            catch (FormatException ex)
            {
                MessageBox.Show("Некорректный ввод! " +
                    "Проверьте данные и повторите попытку", "Ошибка");
                Console.WriteLine(ex.Message);
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Некорректный ввод!");
                Console.WriteLine(ex.Message);
              

            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Result = DialogResult.Cancel;
            MessageBox.Show("Вы отменили действие", "Сообщение");
            Close();
        }
    }
    } 


    

